import request from "@/common/request/index.js";
import store from '@/store';
import base from '@/config/baseUrl';
import {router} from '@/router/index.js' 

let version_code = '';

//可以new多个request来支持多个域名请求
let $http = new request({
	//接口请求地址
	baseUrl: base.baseUrl,
	//服务器本地上传文件地址
	fileUrl: base.baseUrl,
	// 服务器上传图片默认url
	defaultUploadUrl: "/v1/api/upload/image",
	//设置请求头（如果使用报错跨域问题，可能是content-type请求类型和后台那边设置的不一致）
	header: {
		'Content-Type': 'application/json;charset=UTF-8'
	}
});

//请求开始拦截器
$http.requestStart = function(options) {
	// 将common/core/mappingApi.js的代码进行配置注入
	console.log("请求开始", options);

	// 判断是否需要进行登录验证
	if (options && options.auth) {
		// 当前用户没有进行登录操作，直接结束请求
		if (!store.state.tokenInfo.token) {
			// 返回状态码进行捕获
			return "-1"
		}
	}
	//判断url是不是链接
	let urlType = /^(http|https):\/\//.test(options.url);
	// 判断是否是文件类型
	if (options.method == "FILE") {
		options.url = urlType ? options.url : this.fileUrl + options.url;
	} else {
		options.url = urlType ? options.url : this.baseUrl + options.url;
	}

	if (options.load) {
		//打开加载动画
		store.commit("setLoadingShow", true);
	}

	// 图片上传大小限制
	if (options.method == "FILE" && options.maxSize) {
		// 文件最大字节: options.maxSize 可以在调用方法的时候加入参数
		let maxSize = options.maxSize;
		for (let item of options.files) {
			if (item.size > maxSize) {
				setTimeout(() => {
					uni.showToast({
						title: "图片过大，请重新上传",
						icon: "none"
					});
				}, 500);
				return false;
			}
		}
	}
	// #ifdef APP-PLUS
	// 添加当前版本号
	if (version_code) {
		options.header['version_code'] = version_code;
	}
	// #endif
	//请求前加入token
	if (store.state.tokenInfo.token) {
		options.header['Authorization'] = 'Basic ' + store.state.tokenInfo.token;
	};
	return options;
}
//请求结束
$http.requestEnd = function(options) {
	//判断当前接口是否需要加载动画
	if (options.load) {
		// 关闭加载动画
		store.commit("setLoadingShow", false);
	}
}
let loginPopupNum = 0;
//所有接口数据处理（此方法需要开发者根据各自的接口返回类型修改，以下只是模板）
$http.dataFactory = async function(res) {
	console.log("接口请求数据", {
		url: res.url,
		resolve: res.response,
		header: res.header,
		data: res.data,
		method: res.method,
	});
	if (res.response.statusCode && res.response.statusCode == 200) {
		let httpData = res.response.data;
		if (typeof(httpData) == "string") {
			httpData = JSON.parse(httpData);
		}
		// [200:响应正常]
		// [success = false] : 表示错误
		// [401:]
		// [402:用户无权限访问该URL]
		//判断数据是否请求成功
		if (httpData.code == 200) {
			console.log("返回正确");
			// 返回正确的结果(then接受数据)
			return Promise.resolve(httpData);
		} else if ( httpData.code == 401) {
		}  else if (httpData.status == 311) {
			uni.showModal({
				title: '温馨提示',
				content: httpData.message,
				confirmText: '去新增',
				cancelText: "再逛会",
				success: function(res) {
					if (res.confirm) {
						uni.navigateTo({
							url: '/pages/CollectionAddress/CollectionAddress'
						})
					}
				}
			});
		} else { //其他错误提示   
			if (res.isPrompt) {
				uni.showToast({
					title: httpData.message,
					icon: "none",
					duration: 3000
				});
			}
			// 返回错误的结果(catch接受数据)
			return Promise.reject({
				statusCode: 0,
				errMsg: "【request】" + (httpData.message),
				data: res.data
			});
		}
	} else if(res.response.statusCode === 401){
		// 注销用户
		store.commit("emptyUserInfo");
		if (loginPopupNum <= 0) {
			loginPopupNum++;
			uni.showModal({
				title: '温馨提示',
				content: "请登录用户",
				confirmText: "去登录",
				cancelText: "再逛会",
				success: function(res) {
					loginPopupNum--;
					if (res.confirm) {
						// router.push({
						// 	url: "/pages/public/wx_login/wx_login"
						// });
						uni.navigateTo({
							url: "/pages/public/wx_login/wx_login"
						});
						
					}
				}
			});
		}
		return Promise.reject({
			statusCode: 0,
			errMsg: "【request】" + (res.response.data || res.response.message),
			data: res.data
		});
	} else {
		// 返回错误的结果(catch接受数据)
		return Promise.reject({
			statusCode: res.response.statusCode,
			errMsg: "【request】数据工厂验证不通过",
			data: res.data
		});
	}
	
	
};
// 错误回调
$http.requestError = function(e) {
	console.log("重新登录" + JSON.stringify(e));
	console.log("重新登录" + loginPopupNum);
	// e.statusCode === 0 是参数效验错误抛出的
	if (e.statusCode === 0) {
		throw e;
	} else if (e.statusCode === -1 && loginPopupNum <= 0) {
		
		loginPopupNum++;
		uni.showModal({
			title: "提示",
			content: "请登录后再操作~",
			confirmText: "去登录",
			cancelText: "再看看",
			success: (res) => {
				loginPopupNum--;
				if (res.confirm) {
					// #ifdef APP-VUE
					goLeVideoAuth();
					// #endif
				}
			}
		});
	} else {
		console.log(e);
		uni.showToast({
			title: "网络错误，请检查一下网络",
			icon: "none"
		});
	}
}


export default $http
