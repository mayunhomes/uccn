// rsa加密和解密
let res = require('./rsaCode/CryptoJS.js');
const Encrypt = require('./secret library/jsencrypt.min.js');
import {
  hexMD5
} from "./secret library/md5.js";
// 公钥
let publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDFGZ/DoRVke1I+fdTP1zRMOUI4JJ4PYqCySOkznLFdwJP1xtDuDMRSNi3oK2XHpjBwe+dZiBO457jx6QoKaSbq0LDmZvBuVzie3PC/kTAqy2MVOtMczJYGzvX1dXv1EKBsk2PBIPt7J2T5fQXt/3XruIRs4ZfERqrIYjwpxmQ9GQIDAQAB";
// 私钥
let privateKey = 'MIICXgIBAAKBgQC/Su2MIB7m/mVUr3IQiTOW/leezQuo6aqy9w3OxHf+CDk2CrXUjW6tvpRh+3xkQVMSfNjBJmgWkEwhY0Doo8xt30Gjo5KLOyDMGcGTPopZlXKsjrx82Ex56cP1zF7REXKdROA+isPn2dM6hxrH81wRvyg7bx6kvJCAXdxDn67bqQIDAQABAoGBAJD7chU9Fw7OCvDnVFVSF4a+WJxkHg00pIqlkpJ+987SXsIl1fA2DYejpQSqPXoixJqDgyjJOiS30uuPNVfdJYdkJ6iWrkHIO5AAgYALVogkTNXww5SUQRvOpycPdKtHom8satUOFse115/JFIej14f8dFLPSe0LToBj7l9yPwgdAkEA75yjiSbMDWlVlg0Ga+gBci18UuPR56W7lOlXdBKjsU+hfiz4ov/vdIYg431vFvxvhX9TbwdkF2+FIWVJzI81ywJBAMxgROaqhOFRhHCja7mmcb+ZTzCzbyE2jtWQFtsQZKpG9CI5M0D0bv6NETlr+q5027+FUS7zmeeKEjxiLYqApdsCQQDiRFmNx0xDkeeXNZ+NwF21M0zGfYoXJMMZYKwL2Roo8xh+LPtx+RtnPs3J5QZFf4XUW6MvIpybIN1n5t0kxUu3AkBrYcGR+NIsmOofTJ4TQ1LMur6S9hRTvRi7OSPpIeniVbl8KdW2MTgIYxJDu2pRM3EqZd/L7Y5Vy4PdWjBXb1S1AkEAvlsJkUKvq5KEvSJuweB4jtO8TWPVAqpOVVHoeDwudKSxcZQfqnwrE1XGXFGi2G3odAikEjTjyaZ5qsi68Z1yJA==';

function rsaEncrypt(data) {
  // 生成16位随机key
  var str = new Date().valueOf().toString();
  var key = hexMD5(str).substr(9, 16);
  // 使用 对要加密的数据进行对称加密（ecb模式），生成加密数据encryptData，参考https://www.jianshu.com/p/1d079466ece8
  // res是对称加密，用来加密长数据，适合用来加密保密数据参数
  var encryptData = res.Encrypt(data, key);
  console.log('加密后encryptData:', encryptData);
  console.log('解密后encryptData:', res.Decrypt(encryptData, key));
   // 使用ras公钥(rasPublicKey)对aeskey进行加密，得到得到encryptAesKey
    let encryptor = new Encrypt.JSEncrypt();
    encryptor.setPublicKey(publicKey) // 设置公钥
    let encryptAesKey = encryptor.encrypt(key) // 对需要加密的数据进行加密
    console.log("加密后encryptAesKey:", encryptAesKey);
    return {encryptData, encryptAesKey}
    //callback(encryptData, encryptAesKey); // 回调函数
}


function rsaDeEncrypt(encryptAesKey, encryptData) {
   // 解密encryptAesKey
   const decryptor = new Encrypt.JSEncrypt();
   decryptor.setPrivateKey(privateKey); // 设置私钥
   encryptAesKey = decryptor.decrypt(encryptAesKey) // 解密私钥
   console.log("解密后key:", encryptAesKey);
   console.log('解密后encryptData:', res.Decrypt(encryptData, encryptAesKey));
   return res.Decrypt(encryptData, encryptAesKey);
}

export {
  rsaEncrypt,
  rsaDeEncrypt,
}