const RSA = require('../secret library/wx_rsa.js')

export function RSAEncrypt (word, publicKey) {
  let PublicKey = '-----BEGIN PUBLIC KEY-----' + publicKey + '-----END PUBLIC KEY-----'
  let encrypt_rsa = new RSA.RSAKey()
  encrypt_rsa = RSA.KEYUTIL.getKey(PublicKey)
  let encStr = encrypt_rsa.encrypt(word)
  encStr = RSA.hex2b64(encStr)
  return encStr
}

export function RSADecrypt (word, privateKey) {
  let PrivateKey = '-----BEGIN PUBLIC KEY-----' + privateKey + '-----END PUBLIC KEY-----'
  let decrypt_rsa = new RSA.RSAKey()
  decrypt_rsa = RSA.KEYUTIL.getKey(PrivateKey)
  let encStr = RSA.b64tohex(word)
  let decStr = decrypt_rsa.decrypt(encStr)
  console.log('解密结果：' + decStr)
  return decStr
}