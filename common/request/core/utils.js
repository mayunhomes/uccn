import mappingApi from '@/common/request/core/mappingApi.js'

// 获取合并的数据
export const mergeConfig = function(_this, options) {
	let config
	
	if(!(options.method == "FILE")) {
		// 注入配置项
		 let prams = getApiObj(options.url);
		 config = Object.assign({}, _this.config, options, prams ); 
		 if (prams && prams.header) {
		 	config.header = Object.assign({}, _this.header, prams.header);
		 } else {
		 	config.header = _this.header;
		 }
		//请求头
	}else{
		 config = Object.assign({}, _this.config, options ); 
		 config.header = _this.header;
	}
	return config;
}


// 请求
export const dispatchRequest = function(requestInfo) {
	return new Promise((resolve, reject) => {
		let requestData = {
			url: requestInfo.url,
			header: requestInfo.header, //加入请求头
			success: (res) => {
				resolve(res);
			},
			fail: (err) => {
				reject(err);
			}
		};
		
		//请求类型
		if (requestInfo.method) {
			requestData.method = requestInfo.method;
		}
		if (requestInfo.data) {
			requestData.data = requestInfo.data;
		}
		// #ifdef MP-WEIXIN || MP-ALIPAY
		if (requestInfo.timeout) {
			requestData.timeout = requestInfo.timeout;
		}
		// #endif
		if (requestInfo.dataType) {
			requestData.dataType = requestInfo.dataType;
		}
		// #ifndef APP-PLUS || MP-ALIPAY
		if (requestInfo.responseType) {
			requestData.responseType = requestInfo.responseType;
		}
		// #endif
		// #ifdef H5
		if (requestInfo.withCredentials) {
			requestData.withCredentials = requestInfo.withCredentials;
		}
		// #endif
		uni.request(requestData);
	})
}


// 接口解析
function getApiObj(url) {
	let params, flage, cloneObj;
	if(url.indexOf("/") !== -1){
		flage = true;
		params =  url.substr(url.indexOf("/"));
		url = url.substr(0,url.indexOf("/"));
	}
	let apiArray = url.split(".");
	let api = mappingApi;
	apiArray.forEach(v => {
		api = api[v];
	});
	
	cloneObj = JSON.parse(JSON.stringify(api));
	if( Boolean(flage)){	
		cloneObj.url = cloneObj.url + params;
	}
	return  cloneObj;
}