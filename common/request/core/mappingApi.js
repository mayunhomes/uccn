/**
 * 接口列表文件
 */
export default {
	/** 接口测试 ↓ **/
	init: {
		init: {
			url: "/v1/api/public/init",
			auth: false
		}
	},
	// 首页内容
	index: {
		// 首页数据
		index: {
			url: "/v1/api/index/index",
			auth: false
		}
	},
	// 请帖数据
	card: {
		// tab分类数据
		cate: {
			url: "/v1/api/invitation/cate",
			auth: false
		},
		list: {
			url: "/v1/api/invitation/list",
			auth: false
		},
		// 根据id获取模板
		info: {
			url: "/v1/api/invitation/info",
			auth: false
		},
		// 根据订单号获取模板数据
		formwork: {
			url: "/v1/api/public/formwork",
			auth: false
		},
	},
	// 音乐相关接口
	music: {
		// 音乐分类
		cate: {
			url: "/v1/api/music/cate",
			auth: false
		},
		// 音乐列表
		list: {
			url: "/v1/api/music/list",
			auth: false
		}
	},
	// 阅读
	read: {
		article: {
			url: "/v1/api/read/article",
			auth: false
		},
		mv: {
			url: "/v1/api/read/mv",
			auth: false
		},
		scan: {
			url: "/v1/api/read/scan",
			auth: false,
			header: {
				"content-type": "application/x-www-form-urlencoded"
			}
		}
	},
	
	invitation:{
		// 收藏
		collect: {
			url: "/v1/api/invitation/collect",
			auth: false,
			header: {
				"content-type": "application/x-www-form-urlencoded"
			}
		},
		// 支付
		buy: {
			url: "/v1/api/invitation/buy",
			auth: false,
			header: {
				"content-type": "application/x-www-form-urlencoded"
			}
		}
	},
	// 登录
	login: {
		login: {
			url: "/v1/api/public/login",
			auth: false,
			header: {
				"content-type": "application/x-www-form-urlencoded"
			}
		}
	},
	// 用户
	user: {
		index: {
			url: "/v1/api/user/index",
			auth: false,
		},
		// 我的收藏列表
		collectlist: {
			url: "/v1/api/user/collect",
			auth: false,
		},
		// 我的购买
		orderlist:{
			url: "/v1/api/user/order",
			auth: false,
		}
	}
};
