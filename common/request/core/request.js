import {
	mergeConfig,
	dispatchRequest
} from "./utils.js";
export default class request {
	constructor(options) {
		//请求公共地址
		this.baseUrl = options.baseUrl || "";
		//公共文件上传请求地址
		this.fileUrl = options.fileUrl || "";
		// 服务器上传图片默认url
		this.defaultUploadUrl = options.defaultUploadUrl || "";
		//默认请求头
		this.header = options.header || {};
		//默认配置
		this.config = options.config || {
			isPrompt: true,
			load: true,
			isFactory: true,
			resend: 0
		};
	}
	//post请求

	post(url = '', data = {}) {
		return this.request({
			method: "POST",
			data: data,
			url: url
		});
	}

	//get请求
	get(url = '', data = {}) {
		return this.request({
			method: "GET",
			data: data,
			url: url
		});
	}

	//接口请求方法
	async request(data) {
		// 请求数据
		let requestInfo,
			// 是否运行过请求开始钩子
			runRequestStart = false;
		try {
			if (!data.url) {
				throw {
					errMsg: "【request】缺失数据url",
					statusCode: 0
				}
			}
			
			// 数据合并，判断是否有传入参数，如果没有就给定默认值
			requestInfo = mergeConfig(this, data);
			// 代表之前运行到这里
			runRequestStart = true;
			//请求前回调
			if (this.requestStart) {

				//请求开始拦截器
				let requestStart = this.requestStart(requestInfo);
				if (typeof requestStart == "object") {
					// 进行输入同步，除了这些数据，其他的数据都不进行修改，如需要其他参数，请自行添加
					let changekeys = ["data", "header", "isPrompt", "load", "isFactory"];
					changekeys.forEach(key => {
						requestInfo[key] = requestStart[key];
					});
				} else {
					console.log(requestStart);
					if (requestStart === '-1') {
						throw {
							errMsg: "【request】请登陆后在进行操作",
							statusCode: -1,
						}
					} else {
						throw {
							errMsg: "【request】请求开始拦截器未通过",
							statusCode: 0,
							data: requestInfo.data,
							method: requestInfo.method,
							header: requestInfo.header,
							url: requestInfo.url,
						}
					}

				}
			}
			let requestResult = {};
			// 发送请求
			requestResult = await dispatchRequest(requestInfo);
			// 是否用外部的数据处理方法
			if (requestInfo.isFactory && this.dataFactory) {
				//数据处理
				let result = await this.dataFactory({
					...requestInfo,
					response: requestResult
				});
				return Promise.resolve(result);
			} else {
				return Promise.resolve(requestResult);
			}
		} catch (err) {

			this.requestError && this.requestError(err);
			return Promise.reject(err);
		} finally {
			// 如果请求开始未运行到，请求结束也不运行
			if (runRequestStart) {
				// 判断是否有停止动画，如果有则停止
				this.requestEnd && this.requestEnd(requestInfo);
			}
		}
	}
}
