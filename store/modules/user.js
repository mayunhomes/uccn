export const state = {
  //用户数据
  userInfo: {},
  // 登录数据
  tokenInfo: {},
};
export const mutations = {
  //储存用户信息
  setUserInfo(state, data) {
  	if (data) {
  		state.userInfo =  Object.assign({}, state.userInfo,data);
  		// #ifdef H5
  		window.sessionStorage.setItem('userInfo', JSON.stringify(state.userInfo));
  		// #endif
  		// #ifndef H5
  		uni.setStorageSync('userInfo', state.userInfo);
  		// #endif
  	}
  },
  //储存登录数据
  setTokenInfo(state, data) {
  	if (data) {
  		state.tokenInfo =  Object.assign({}, state.tokenInfo,data);
  		// #ifdef H5
  		window.sessionStorage.setItem('tokenInfo', JSON.stringify(state.tokenInfo));
  		// #endif
  		// #ifndef H5
  		uni.setStorageSync('tokenInfo', state.tokenInfo);
  		// #endif
  	}
  },
  // 退出登录 
  emptyUserInfo(state) {
  	state.userInfo = {};
  	// #ifdef H5
  	window.sessionStorage.removeItem("userInfo");
  	// #endif
  	// #ifndef H5
  	uni.removeStorageSync("userInfo");
  	// #endif
  },
  // 退出token
  emptyTokenInfo(state) {
  	state.tokenInfo = {};
  	// #ifdef H5
  	window.sessionStorage.removeItem("tokenInfo");
  	// #endif
  	// #ifndef H5
  	uni.removeStorageSync("tokenInfo");
  	// #endif
  },
};
export const actions = {
  
};
