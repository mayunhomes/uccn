
import Vue from 'vue'
import App from './App'

import i18n from '@/lang/index.js' //引入配置文件
Vue.prototype._i18n  = i18n;
import uView from "uview-ui";
Vue.use(uView);
import publicModule from "@/components/commons/public-module.vue";
import MescrollBody from "@/components/mescroll-uni/mescroll-body.vue"
import MescrollUni from "@/components/mescroll-uni/mescroll-uni.vue"

//数据管理中心
import store from '@/store'
Vue.prototype.$store = store;
//权限配置中心
import base from '@/config/baseUrl'
Vue.prototype.$base = base;
//挂载全局http请求
import $http from '@/config/requestConfig'
Vue.prototype.$http = $http;
// util
import $utils from '@/common/utils/tools.js'
Vue.prototype.$utils = $utils;

// 路由拦截
import {router,RouterMount} from './router/index.js'  //路径换成自己的
Vue.use(router)

Vue.config.productionTip = false

App.mpType = 'app'


Vue.component("public-module", publicModule);
Vue.component('mescroll-body', MescrollBody)
Vue.component('mescroll-uni', MescrollUni)

Vue.prototype.tabbar = [
	{
		iconPath: "/static/images/Bashbet.png",
		selectedIconPath: "/static/images/Bashbetactive.png",
		text: '首页',
		customIcon: false,
		pagePath: "/pages/index/index",
	},
	{
		iconPath: "/static/images/Mezmunlar.png",
		selectedIconPath: "/static/images/Mezmunlaractive.png",
		text: '内容',
		customIcon: false,
		pagePath: "/pages/content/content",
	},
	{
		iconPath: "/static/images/Baghaqlar.png",
		selectedIconPath: "/static/images/Baghaqlaractive.png",
		text: '请帖',
		customIcon: false,
		pagePath: "/pages/card/card",
	},
	{
		iconPath: "/static/images/Mining.png",
		selectedIconPath: "/static/images/Miningactive.png",
		text: '我的', 
		customIcon: false,
		pagePath: "/pages/personalFile/personalFile",
	},
];

const app = new Vue({
	  i18n,
    ...App
})
// #ifdef H5
import wxsdk from '@/common/wechat/sdk'

Vue.prototype.$wxsdk = wxsdk;
	RouterMount(app,router,'#app')
// #endif

// #ifndef H5
	app.$mount(); 
// #endif
