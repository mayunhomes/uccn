// index.js

import Vue from 'vue'
import VueI18n from 'vue-i18n';
import {lang} from './zh-CN.js';
import {lang2} from './zh-WH.js';
// import {lang} from './zh-CN.js';
Vue.use(VueI18n)
    const i18n = new VueI18n({
        locale: 'zh_CN',  // 默认选择的语言
        messages: {  
                'zh_CN': lang,
								'zh_WH': lang2,
            }
    })
    export default i18n