export const lang = {
	tabBar: {
		index: "首页",
		content: "内容",
		card: "请帖",
		personalFile: "我的"
	},
	// 首页
	index: {
		title: "首页",
		headerleft1: "更多",
		headerright1: "精选请帖",
		headerleft2: "更多",
		headerright2: "精选MV",
		date: "时间",
		longtime: "视频时长",
		headerleft3: "更多",
		headerright3: "精选阅读",
		edit: "编辑",
		auth: "作者"
	},
	// 内容
	content: {
		title: "内容",
		headerleft1: '精选MV',
		headerright1: '精选阅读',
	},
	// 我的
	personalFile: {
		title: "我的",
		shop: "我的购买",
		card: "我的请帖",
		gongzhonghao: "公众号",
		banzhu: "帮助",
		shouchang: "我的收藏"
	},
	// 弹出框
	total: {
		concel: "取消",
		concom: "确定",
		title: "请先登录"
	},
	// 公用
	commons: {
		perfile: "上一步",
		save: "保存",
		next: "下一步"
	}
}
